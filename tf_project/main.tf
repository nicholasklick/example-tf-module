terraform {
    backend "http" {}
}

module "my_module" {
  source = "gitlab.com/nagyv-gitlab/my-module/my-system"
  random_seed = "Hello world!"
}

output "child_output" {
    value = module.my_module.output
}
