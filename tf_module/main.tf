terraform {
  required_providers {
    random = {
      source  = "hashicorp/random"
      version = "3.1.0"
    }
  }
  required_version = ">= 0.15"
}

variable "random_seed" {
  type = string
}

resource "random_integer" "sequence" {
  min  = 1
  max  = 1000
  seed = var.random_seed
}

output "output" {
  value = random_integer.sequence.result
}
